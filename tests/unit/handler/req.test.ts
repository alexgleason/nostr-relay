import { WebSocket } from 'mock-socket';

import mindsService from '../../../src/service/minds';
import reqHandler from '../../../src/handler/req';
import mockEntity from '../../testdata/minds/entity';
import Subscription from '../../../src/model/subscription';

// Mocks
const errorSpy = jest.spyOn(console, 'error').mockImplementation();
const ws = new WebSocket('ws://localhost:8080');

jest.mock('../../../src/service/minds');
const mockService = mindsService as jest.Mocked<typeof mindsService>;

const mockReq = {
  authors: ['687cae10951e5c7aace29ed05e2021ba286f1a3a7dc74627330627e242f3ad8f']
};

const subs = new Map();
const sub = new Subscription();
sub.lastEvent = 1659662497;
subs.set('subscription-id', sub);

// Spy
const sendSpy = jest.spyOn(ws, 'send');

describe('REQ Handler Tests', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should call the Minds service when given a valid subscription id', () => {
    mockService.getReq.mockResolvedValue(mockEntity);

    reqHandler(ws, 'subscription-id', mockReq, subs).then(() => {
      expect(mockService.getReq).toHaveBeenCalled();
    });
  });

  it('should send the stringified Minds post over the WebSocket', () => {
    mockService.getReq.mockResolvedValue(mockEntity);

    sub.lastEvent = 1652765937;
    subs.set('subscription-id', sub);

    reqHandler(ws, 'subscription-id', mockReq, subs).then(() => {
      expect(sendSpy).toHaveBeenCalledWith(
        JSON.stringify(['EVENT', 'subscription-id', mockEntity[1]])
      );
      expect(sendSpy).toHaveBeenCalledWith(
        JSON.stringify(['EVENT', 'subscription-id', mockEntity[2]])
      );
    });
  });

  it('should send the EOSE message over the WebSocket', () => {
    mockService.getReq.mockResolvedValue(mockEntity);

    reqHandler(ws, 'subscription-id', mockReq, subs).then(() => {
      expect(sendSpy).toHaveBeenCalledWith(
        JSON.stringify(['EOSE', 'subscription-id'])
      );
    });
  });

  it('should only return new events for an active subscription', () => {
    mockService.getReq.mockResolvedValue(mockEntity);

    sub.lastEvent = 1659662497;
    subs.set('subscription-id', sub);

    reqHandler(ws, 'subscription-id', mockReq, subs).then(() => {
      expect(sendSpy).toHaveBeenCalledTimes(2);
    });
  });
});
