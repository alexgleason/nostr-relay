import axios from 'axios';
import { formatNotice, formatOk, formatNotOk } from '../helper/format-event';
import Event from '../model/event';

const baseUri = process.env.MINDS_BASE_URI;

const getReq = async (ws: WebSocket, filters: string[]) => {
  try {
    // Get the events from the engine endpoint
    const response = await axios.get(`${baseUri}/api/v3/nostr/req`, {
      params: { filters }
    });
    return response.data;
  } catch (err) {
    console.error(
      `[ERROR]: Error when retrieving events from Minds backend! Status: ${err.response?.status}, Message: ${err.response?.data?.message}`
    );
    ws.send(
      formatNotice(
        '[ERROR]: Failed to retrieve events for the provided filters!'
      )
    );
  }
};

const putEvent = async (ws: WebSocket, event: Event) => {
  try {
    // Get the event from the engine endpoint
    const response = await axios.put(`${baseUri}/api/v3/nostr/event`, event);

    // send nip20 result
    ws.send(formatOk(event.id));

    return response.data;
  } catch (err) {
    console.error(
      `[ERROR]: Error when persisting event to Minds backend! Status: ${err.response?.status}, Message: ${err.response?.data?.message}`
    );

    if (err.response) {
      ws.send(formatNotice(`[ERROR]: ${err?.message}`));
      ws.send(formatNotOk(event.id, err?.message));
    } else {
      ws.send(
        formatNotOk(
          event.id,
          'error: Failed to persist event to Minds backend!'
        )
      );
    }
  }
};

export default {
  getReq,
  putEvent
};
