import mindsService from '../service/minds';
import Event from '../model/event';

const eventHandler = async (ws: WebSocket, event: Event) => {
  // TODO: any errors that need to be thrown?
  await mindsService.putEvent(ws, event);
};

export default eventHandler;
