import mindsService from '../service/minds';
import { formatEose, formatEvent } from '../helper/format-event';
import Subscription from '../model/subscription';

const reqHandler = async (
  ws: WebSocket,
  subscriptionId: string,
  filters: any,
  subs: Map<string, Subscription>
) => {
  // Retrieve events from Minds backend
  const response = await mindsService.getReq(ws, filters);

  if (!response) {
    return;
  }

  // Sort by created_at
  if (Array.isArray(response)) {
    response.sort((a, b) => a.created_at - b.created_at);
  }

  // Retrieve state for the active subscription
  const active = subs.get(subscriptionId);

  // Filter for events signed after those already sent
  const events = response.filter((e) => e.created_at > active?.lastEvent || 0);

  // If new events have been discovered, update our position in the feed
  const lastEvent = events[events.length - 1];
  if (lastEvent) {
    active.lastEvent = lastEvent.created_at;
    subs.set(subscriptionId, active);
  }

  // Send all events we found for the provided filters
  (events || []).forEach((event) => {
    if (event) {
      console.log(`[SENT]: ${formatEvent(subscriptionId, event)}`);
      ws.send(formatEvent(subscriptionId, event));
    }
  });

  // Finally, send the end-of-stored-events message (NIP 15)
  ws.send(formatEose(subscriptionId));
};

export default reqHandler;
