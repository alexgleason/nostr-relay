import { WebSocket } from 'mock-socket';

import eventHandler from '../../../src/handler/index';
import reqHandler from '../../../src/handler/req';
import Subscription from '../../../src/model/subscription';

// Mocks
jest.mock('../../../src/handler/req');
const mockHandler = reqHandler as jest.Mocked<typeof reqHandler>;

const ws = new WebSocket('ws://localhost:8080');

const promMock = { inc: jest.fn() };

// Spy
const errorSpy = jest.spyOn(console, 'error').mockImplementation();
const sendSpy = jest.spyOn(ws, 'send');

describe('Event Handler Tests', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should log a console error when passed a non-JSON message', () => {
    eventHandler(ws, 'notjson', new Map(), promMock, promMock, promMock);
    expect(errorSpy).toHaveBeenCalled();
  });

  it('should not log a console error when passed a valid JSON message', () => {
    eventHandler(
      ws,
      '["REQ", "session-id", { "ids": ["event-id"] }]',
      new Map(),
      promMock,
      promMock,
      promMock
    );
    expect(errorSpy).not.toHaveBeenCalled();
  });

  it('should call the reqHandler when given a message with the REQ key', () => {
    eventHandler(ws, '["REQ", "session-id", {}]', new Map(), promMock, promMock, promMock);
    expect(mockHandler).toHaveBeenCalled();
  });

  it('should send a notice to the client when the incoming message cannot be parsed as JSON', () => {
    eventHandler(ws, 'notjson', new Map(), promMock, promMock, promMock);
    expect(sendSpy).toBeCalledWith(expect.stringContaining('NOTICE'));
  });

  it('should close the subscription when given a message with a CLOSE key', () => {
    const subs = new Map();
    subs.set('session-id', new Subscription());
    eventHandler(ws, '["CLOSE", "session-id", {}]', subs, promMock, promMock, promMock);

    expect(subs.get('session-id')).toBeUndefined();
  });

  it('should send a notice to the client when the incoming message does not have a supported NOSTR key', () => {
    eventHandler(ws, '["KEY", "session-id", {}]', new Map(), promMock, promMock, promMock);
    expect(sendSpy).toBeCalledWith(expect.stringContaining('NOTICE'));
  });
});
