import mockEntity from '../../../testdata/minds/entity';
import {
  formatEvent,
  formatNotice,
  formatOk,
  formatNotOk
} from '../../../../src/helper/format-event';

describe('Event Formatter Tests', () => {
  it('should return a formatted EVENT payload', () => {
    expect(formatEvent('subscription-id', mockEntity[0])).toEqual(
      JSON.stringify(['EVENT', 'subscription-id', mockEntity[0]])
    );
  });

  it('should return a formatted NOTICE payload', () => {
    expect(formatNotice('Oops! Encountered an error!')).toEqual(
      JSON.stringify(['NOTICE', 'Oops! Encountered an error!'])
    );
  });

  it('should return formatted OK payloads', () => {
    expect(formatOk(mockEntity[0].id)).toEqual(
      JSON.stringify(['OK', mockEntity[0].id, true, ''])
    );

    expect(formatNotOk(mockEntity[0].id, 'error: fruit')).toEqual(
      JSON.stringify(['OK', mockEntity[0].id, false, 'error: fruit'])
    );
  });
});
